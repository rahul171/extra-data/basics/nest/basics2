import { Module } from '@nestjs/common';
import { AppController } from './controllers/app.controller';
import { MessageService } from './services/message/message.service';
import { UtilService } from './services/util/util.service';
import { RequestProviderScopeController } from './controllers/ProviderScope/request.providerScope.controller';
import {
  TransientScopeController1,
  TransientScopeController2,
} from './controllers/ProviderScope/transient.providerScope.controller';
import { RequestProviderScopeService } from './services/ProviderScope/request.providerScope.service';
import { TransientProviderScopeService } from './services/ProviderScope/transient.providerScope.service';
import {
  SingletonScopeController1,
  SingletonScopeController2,
} from './controllers/ProviderScope/singleton.providerScope.controller';
import { SingletonProviderScopeService } from './services/ProviderScope/singleton.providerScope.service';
import { RequestControllerScopeController } from './controllers/ControllerScope/request.controllerScope.controller';

@Module({
  controllers: [
    AppController,
    RequestProviderScopeController,
    TransientScopeController1,
    TransientScopeController2,
    SingletonScopeController1,
    SingletonScopeController2,
    RequestControllerScopeController,
  ],
  providers: [
    MessageService,
    UtilService,
    RequestProviderScopeService,
    TransientProviderScopeService,
    SingletonProviderScopeService,
  ],
})
export class AppModule {}
