import { AppController } from './app.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { MessageService } from '../services/message/message.service';
import { UtilService } from '../services/util/util.service';

describe('App Controller', () => {
  let controller: AppController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [MessageService, UtilService],
    }).compile();

    controller = module.get<AppController>(AppController);
  });

  it('Should return welcome message', () => {
    expect(controller.welcomeMessage()).toEqual(expect.any(String));
  });

  it('Should return random number', () => {
    const random = controller.randomNumber(1, 10);
    expect(random).toBeGreaterThanOrEqual(1);
    expect(random).toBeLessThanOrEqual(10);
  });
});
