import { Controller, Get, Param } from '@nestjs/common';
import { SingletonProviderScopeService } from '../../services/ProviderScope/singleton.providerScope.service';

@Controller({
  path: 'singleton1-provider-scope',
})
export class SingletonScopeController1 {
  constructor(private singletonScopeService: SingletonProviderScopeService) {}

  @Get('add/:value')
  add(@Param('value') value): number[] {
    this.singletonScopeService.addValue(parseInt(value));
    return this.singletonScopeService.getValues();
  }
}

@Controller({
  path: 'singleton2-provider-scope',
})
export class SingletonScopeController2 {
  constructor(private singletonScopeService: SingletonProviderScopeService) {}

  @Get('add/:value')
  add(@Param('value') value): number[] {
    this.singletonScopeService.addValue(parseInt(value));
    return this.singletonScopeService.getValues();
  }
}
