import { Controller, Get, Param } from '@nestjs/common';
import { RequestProviderScopeService } from '../../services/ProviderScope/request.providerScope.service';

@Controller({
  path: 'request-provider-scope',
})
export class RequestProviderScopeController {
  constructor(private requestScopeService: RequestProviderScopeService) {
    console.log('RequestProviderScopeController => constructor');
  }

  @Get('add/:value')
  add(@Param('value') value): number[] {
    this.requestScopeService.addValue(parseInt(value));
    return this.requestScopeService.getValues();
  }
}
