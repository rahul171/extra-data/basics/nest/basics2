import { Controller, Get, Param } from '@nestjs/common';
import { TransientProviderScopeService } from '../../services/ProviderScope/transient.providerScope.service';

@Controller({
  path: 'transient1-provider-scope',
})
export class TransientScopeController1 {
  constructor(private transientScopeService: TransientProviderScopeService) {}

  @Get('add/:value')
  add(@Param('value') value): number[] {
    this.transientScopeService.addValue(parseInt(value));
    return this.transientScopeService.getValues();
  }
}

@Controller({
  path: 'transient2-provider-scope',
})
export class TransientScopeController2 {
  constructor(private transientScopeService: TransientProviderScopeService) {}

  @Get('add/:value')
  add(@Param('value') value): number[] {
    this.transientScopeService.addValue(parseInt(value));
    return this.transientScopeService.getValues();
  }
}
