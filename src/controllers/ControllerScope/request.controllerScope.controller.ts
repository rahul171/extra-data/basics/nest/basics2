import { Controller, Get, Param, Scope } from '@nestjs/common';

@Controller({
  path: 'request-controller-scope',
  scope: Scope.REQUEST,
})
export class RequestControllerScopeController {
  private readonly values: number[];

  constructor() {
    console.log('RequestControllerScopeController => constructor');
    this.values = [];
  }

  @Get()
  getValues(): number[] {
    return this.values;
  }

  @Get('/add/:value')
  addValue(@Param('value') value): number {
    value = parseInt(value);
    this.values.push(value);
    return value;
  }
}
