import { Controller, Get, Param } from '@nestjs/common';
import { MessageService } from '../services/message/message.service';
import { UtilService } from '../services/util/util.service';

@Controller()
export class AppController {
  constructor(
    private messageService: MessageService,
    private utilService: UtilService,
  ) {}

  @Get()
  welcomeMessage(): string {
    return this.messageService.getWelcomeMessage();
  }

  @Get('random/:a?/:b?')
  randomNumber(@Param('a') a, @Param('b') b): number {
    return this.utilService.generateRandomNumber(parseInt(a), parseInt(b));
  }
}
