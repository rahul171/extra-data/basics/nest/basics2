import { Test, TestingModule } from '@nestjs/testing';
import { MessageService } from './message.service';

describe('Message Service', () => {
  let service: MessageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MessageService],
    }).compile();

    service = module.get<MessageService>(MessageService);
  });

  it('Should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should return welcome message', () => {
    expect(service.getWelcomeMessage().length).toBeGreaterThan(0);
  });
});
