import { Injectable } from '@nestjs/common';

@Injectable()
export class MessageService {
  getWelcomeMessage(): string {
    return 'Welcome to the Jungle';
  }
}
