import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

@Injectable({
  scope: Scope.REQUEST,
})
export class RequestProviderScopeService {
  private values: number[] = [];

  // https://docs.nestjs.com/fundamentals/injection-scopes#performance

  // constructor(@Inject(REQUEST) private request: Request) {
  //   console.log('RequestProviderScopeService => constructor', request.url);
  // }

  constructor() {
    console.log('RequestProviderScopeService => constructor');
  }

  getValues(): number[] {
    return this.values;
  }

  addValue(value: number): number {
    return this.values.push(value);
  }
}
