import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

@Injectable({
  scope: Scope.TRANSIENT,
})
export class TransientProviderScopeService {
  private values: number[] = [];

  // injecting a request param won't create an instance of this class when app is started,
  // instead, it will create one every time a route is visited.
  // so basically, it works like a REQUEST scope.
  // https://docs.nestjs.com/fundamentals/injection-scopes#performance

  // constructor(@Inject(REQUEST) private request: Request) {
  //   console.log('TransientProviderScopeService => constructor', request.url);
  // }

  constructor() {
    console.log('TransientProviderScopeService => constructor');
  }

  getValues(): number[] {
    return this.values;
  }

  addValue(value: number): number {
    return this.values.push(value);
  }
}
