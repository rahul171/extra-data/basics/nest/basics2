import { Test, TestingModule } from '@nestjs/testing';
import { UtilService } from './util.service';

describe('Util Service', () => {
  let service: UtilService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UtilService],
    }).compile();

    service = module.get<UtilService>(UtilService);
  });

  it('Should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should generate random number', () => {
    const random1 = service.generateRandomNumber();
    expect([0, 1].includes(random1)).toBe(true);

    const random2 = service.generateRandomNumber(5, 10);
    expect(random2).toBeGreaterThanOrEqual(5);
    expect(random2).toBeLessThanOrEqual(10);
  });
});
