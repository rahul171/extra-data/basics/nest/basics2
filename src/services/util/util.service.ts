import { Injectable } from '@nestjs/common';

@Injectable()
export class UtilService {
  generateRandomNumber(a: number = 0, b: number = 1): number {
    return a + Math.floor((b - a + 1) * Math.random());
  }
}
